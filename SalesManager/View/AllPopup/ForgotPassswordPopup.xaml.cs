﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SalesManager.View.AllPopup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ForgotPassswordPopup : Rg.Plugins.Popup.Pages.PopupPage
    {
        public ForgotPassswordPopup()
        {
            InitializeComponent();
        }
    }
}