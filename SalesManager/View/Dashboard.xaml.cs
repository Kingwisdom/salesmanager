﻿using Rg.Plugins.Popup.Extensions;
using SalesManager.Model.APIResponseObjects;
using SalesManager.View.AllPopup;
using SalesManager.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SalesManager.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Dashboard : ContentPage
    {
        DashBoardVM DBVM;
        public Dashboard()
        {
            InitializeComponent();
            BindingContext = DBVM = new DashBoardVM();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                await Navigation.PushPopupAsync(new ActivityPopup());
                await DBVM.GetBalance();
                await DBVM.GetAgentbysprint();
                await DBVM.GetAgentChurch();
                if (Application.Current.Properties.ContainsKey("AgentsChurch"))
                {
                    DBVM.getrecents();
                }
              
              
            }
            catch
            {
                await DisplayAlert("Connection error", "To Sync Please connect to wifi/data","ok");
            }
            finally
            {
                try
                {
                    await Navigation.PopPopupAsync();
                }
                catch
                {

                }
            }
        }

        private async void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if(e.Item == null)
            {
                return;
            }
           
            var itemtap = (AgentsLeadResObject)((ListView)sender).SelectedItem;
            ((ListView)sender).SelectedItem = null;
            await Navigation.PushAsync(new LeadsDetailPage(itemtap.LeadId));
           
        }

        private void AddChurch_Clicked(object sender, EventArgs e)
        {
            //ChurchVisited()
            Application.Current.MainPage.Navigation.PushAsync(new ChurchVisited()); //= new NavigationPage(new ChurchVisited());
        }
    }
}