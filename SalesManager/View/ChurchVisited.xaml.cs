﻿using SalesManager.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SalesManager.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChurchVisited : ContentPage
    {
        ChurchVisitedVM CVVM => BindingContext as ChurchVisitedVM;
        public ChurchVisited()
        {
            InitializeComponent();
            if (CVVM == null)
            {
                BindingContext  = new ChurchVisitedVM(this.Navigation);
            }
            else
            {
                return;
            }
        }
    }
}