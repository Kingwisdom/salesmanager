﻿using Autofac;
using SalesManager.Dependencies;
using SalesManager.Services.AuthServices;
using SalesManager.ViewModel.AuthVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SalesManager.View.Authentication
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginviewMain : ContentPage
    {
        LoginVM lVM;
        public LoginviewMain()
        {
            InitializeComponent();

            //using (var scope = AllDependencies.container.BeginLifetimeScope())
            //{
            //    lVM = AllDependencies.container.Resolve<LoginVM>();

            //}
            lVM = new LoginVM(this.Navigation, new loginServices());
            BindingContext = lVM;
        }

        private void change_Tapped(object sender, EventArgs e)
        {
            Password.IsPassword = Password.IsPassword ? false : true;
        }
    }
}