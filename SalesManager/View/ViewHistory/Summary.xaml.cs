﻿using SalesManager.ViewModel.HistoryVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SalesManager.View.ViewHistory
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Summary : ContentPage
    {
        SummaryVM SVM;
        public Summary()
        {
            InitializeComponent();
            BindingContext = SVM = new SummaryVM(this.Navigation);
        }
    }
}