﻿using SalesManager.Model.APIResponseObjects;
using SalesManager.ViewModel.HistoryVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SalesManager.View.ViewHistory
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MonthsVisits : ContentPage
    {
        TodaysVisitsVM TVVM;
        public MonthsVisits(int position)
        {
            InitializeComponent();
            BindingContext = TVVM = new TodaysVisitsVM(position);
        }

        private async void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return;
            }
            var itemtap = (AgentsLeadResObject)((ListView)sender).SelectedItem;
            ((ListView)sender).SelectedItem = null;
            await Navigation.PushAsync(new LeadsDetailPage(itemtap.LeadId));
        }
    }
}