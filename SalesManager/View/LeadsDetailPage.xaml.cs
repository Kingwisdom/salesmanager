﻿using SalesManager.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SalesManager.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LeadsDetailPage : ContentPage
    {
        private Guid Id;
        public LeadsDetailPage(Guid guid)
        {
            InitializeComponent();
            Id = guid;


            BindingContext = new LeadsDetailVM(Id);
        }
    }
}