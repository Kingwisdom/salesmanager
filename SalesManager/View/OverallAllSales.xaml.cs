﻿using SalesManager.ViewModel.HistoryVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SalesManager.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OverallAllSales : ContentPage
    {
        TodaysVisitsVM TVVM;
        public OverallAllSales(int position)
        {
            InitializeComponent();
            BindingContext = TVVM = new TodaysVisitsVM(position);
        }
    }
}