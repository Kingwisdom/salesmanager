﻿using SalesManager.ViewModel.WithDrawVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SalesManager.View.Withdraw
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WithdrawalPage : ContentPage
    {
        public WithdrawalPage()
        {
            InitializeComponent();
            BindingContext = new WithdrawViewModel();
        }
    }
}