﻿using SalesManager.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SalesManager.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyTabbedPage : TabbedPage
    {
        public MyTabbedPage()
        {
            InitializeComponent();

            NavigationPage ChurchVisited = new NavigationPage(new ChurchVisited());
            //   navigationPage.IconImageSource = "schedule.png";
            ChurchVisited.Title = "ChurchVisited";


            Children.Add(new Dashboard());
            //Children.Add(new ChurchVisited());

           
        }
    }
}