﻿using SalesManager.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesManager.Services
{
    public class ChurchSizePicker
    {
        public static List<ChurchSize> churchSizes()
        {
            var sizes = new List<ChurchSize>()
            { 
                new ChurchSize(){ Key=1, Size="1 - 100"}, 
                new ChurchSize(){ Key=2, Size="101 -300"}, 
                new ChurchSize(){ Key=3, Size="301 - 500"}, 
                new ChurchSize(){ Key=4, Size="501 - 1000"}, 
                new ChurchSize(){ Key=5, Size="1001 - 5000"}, 
                new ChurchSize(){ Key=6, Size="5001 - 10000"}, 
                new ChurchSize(){ Key=7, Size="100001 - 100000"}, 
                new ChurchSize(){ Key=8, Size="> 100000"}, 
            };

            return sizes;
        }
    }
}
