﻿using SalesManagerChurchRequestObjects;
using SalesManagerWithdrawModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesManager.Services.AgentServices
{
    public interface IAgentService
    {
        Task<bool> Getagent<T>();
        Task<bool> Getagentsprint<T>();
        Task<bool> PostChurch<T>(ChurchRequestObjects CRO);
        Task<bool> GetBalance<T>();
        Task<bool> WithdrawBalance<T>(WithdrawModel WDM);
        Task<bool> Getagentschurch<T>();
    }
}
