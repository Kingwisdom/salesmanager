﻿using Newtonsoft.Json;
using SalesManager.Constants;
using SalesManager.Model.APIResponseObjects;
using SalesManager.Services.RepoServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SalesManager.Services.AuthServices
{
    class loginServices : ILoginService
    {
        IGenericRepository genericRepository;
        public loginServices()
        {
            genericRepository = new Genericrepository();
        }

        public async Task<bool> forgotPassword<T>(string email)
        {
            var url = ApiConstants.forgotpassword;
            var userlogin = new ForgotPasswordRequest
            {
                Email = email,
            };
            var success = await genericRepository.PostAsync<T, ForgotPasswordRequest>(url, userlogin);
            if (success != null)
            {
                var user = JsonConvert.SerializeObject(success);
                Application.Current.Properties["user"] = user;
                await Application.Current.SavePropertiesAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> Login<T>(string email, string password)
        {
            var url = ApiConstants.login;
            var userlogin = new LoginRequest
            {
                UserName = email,
                Password = password,
            };
            var success = await genericRepository.PostAsync<T, LoginRequest>(url, userlogin);
            if(success != null)
            {
                var user = JsonConvert.SerializeObject(success);
                Application.Current.Properties["user"] = user;
                await Application.Current.SavePropertiesAsync();
                return true;
            }
            return false;
        }
    }
}
