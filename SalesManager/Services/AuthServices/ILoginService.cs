﻿using SalesManager.Model.APIResponseObjects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesManager.Services.AuthServices
{
    interface ILoginService
    {
        Task<bool> Login<T>(string email, string password);
        Task<bool> forgotPassword<T>(string email);
    }
}
