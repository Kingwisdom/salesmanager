﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesManager.Model
{
    public class ChurchSize
    {
        public int Key { get; set; }
        public string Size { get; set; }
    }
}
