﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SQLite;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace SalesManagerChurchRequestObjects
{
    public partial class ChurchRequestObjects
    {
        [JsonProperty("leadId")]
        public Guid LeadId { get; set; }

        [JsonProperty("churchName")]
        public string ChurchName { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("churchPhoneNumber")]
        public string ChurchPhoneNumber { get; set; }

        [JsonProperty("pastorsphonenumber")]
        public string Pastorsphonenumber { get; set; }

        [JsonProperty("picture")]
        public string Picture { get; set; }

        [JsonProperty("latitude")]
        public double Latitude { get; set; }

        [JsonProperty("longitude")]
        public double Longitude { get; set; }

        [JsonProperty("altitude")]
        public double? Altitude { get; set; }

        [JsonProperty("churchContactName")]
        public string ChurchContactName { get; set; }

        [JsonProperty("website")]
        public string Website { get; set; }

        [JsonProperty("churchSize")]
        public string ChurchSize { get; set; }

        [JsonProperty("town")]
        public string Town { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("deal")]
        public long Deal { get; set; }

        [JsonProperty("agentId")]
        public string AgentId { get; set; }

        [JsonProperty("contactDate")]
        public DateTimeOffset ContactDate { get; set; }

        [JsonProperty("dateVerified")]
        public DateTimeOffset DateVerified { get; set; }

        [JsonProperty("verifiedBy")]
        public string VerifiedBy { get; set; }

        [JsonProperty("followUps")]
        public List<Guid> FollowUpsId { get; set; }
    }

    public partial class FollowUp
    {
        [JsonProperty("followUpId")]
        public Guid FollowUpId { get; set; }

        [JsonProperty("leadId")]
        public Guid LeadId { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("dateOfAction")]
        public DateTimeOffset DateOfAction { get; set; }

        [JsonProperty("remark")]
        public string Remark { get; set; }

        [JsonProperty("followupBy")]
        public string FollowupBy { get; set; }
    }

    public partial class ChurchRequestObjects
    {
        public static ChurchRequestObjects FromJson(string json) => JsonConvert.DeserializeObject<ChurchRequestObjects>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this ChurchRequestObjects self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}