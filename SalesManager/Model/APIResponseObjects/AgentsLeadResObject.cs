﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesManager.Model.APIResponseObjects
{
    public class AgentsLeadResObject
    {
        [JsonProperty("leadId")]
        public Guid LeadId { get; set; }

        [JsonProperty("churchName")]
        public string ChurchName { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("churchPhoneNumber")]
        public string ChurchPhoneNumber { get; set; }

        [JsonProperty("pastorsphonenumber")]
        public string Pastorsphonenumber { get; set; }

        [JsonProperty("picture")]
        public string Picture { get; set; }

        [JsonProperty("latitude")]
        public long? Latitude { get; set; }

        [JsonProperty("longitude")]
        public long? Longitude { get; set; }

        [JsonProperty("altitude")]
        public long? Altitude { get; set; }

        [JsonProperty("churchContactName")]
        public string ChurchContactName { get; set; }

        [JsonProperty("website")]
        public string Website { get; set; }

        [JsonProperty("churchSize")]
        public string ChurchSize { get; set; }

        [JsonProperty("town")]
        public string Town { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
        
        public string Statusstr
        {
            get
            {
                if(Deal == 1)
                {
                    return "Sold";
                }
                else
                {
                    return "Pending";
                }
            }
        }

        [JsonProperty("deal")]
        public long Deal { get; set; }

        [JsonProperty("agentId")]
        public Guid AgentId { get; set; }

        [JsonProperty("contactDate")]
        public DateTime ContactDate { get; set; }

        [JsonProperty("dateVerified")]
        public DateTime DateVerified { get; set; }

        [JsonProperty("verifiedBy")]
        public string VerifiedBy { get; set; }

        [JsonProperty("followUps")]
        public FollowUp[] FollowUps { get; set; }
    }

    public partial class FollowUp
    {
        [JsonProperty("followUpId")]
        public Guid FollowUpId { get; set; }

        [JsonProperty("leadId")]
        public Guid LeadId { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("dateOfAction")]
        public DateTimeOffset DateOfAction { get; set; }

        [JsonProperty("remark")]
        public string Remark { get; set; }

        [JsonProperty("followupBy")]
        public string FollowupBy { get; set; }
    }
}
