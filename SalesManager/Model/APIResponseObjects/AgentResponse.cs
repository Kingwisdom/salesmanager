﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace SalesManagerAgentResponse
{
    public partial class AgentResponse
    {
        [JsonProperty("agentId")]
        public Guid AgentId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("dateOfBirth")]
        public string DateOfBirth { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("passportPhoto")]
        public string PassportPhoto { get; set; }

        [JsonProperty("dateOfAppointment")]
        public DateTimeOffset DateOfAppointment { get; set; }

        [JsonProperty("isActive")]
        public bool IsActive { get; set; }

        [JsonProperty("catchmentId")]
        public Guid CatchmentId { get; set; }

        [JsonProperty("catchment")]
        public Catchment Catchment { get; set; }

        [JsonProperty("numberOfLeads")]
        public long NumberOfLeads { get; set; }
    }

    public partial class Catchment
    {
        [JsonProperty("catchmentId")]
        public Guid CatchmentId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("agents")]
        public List<object> Agents { get; set; }
    }

    public partial class AgentResponse
    {
        public static AgentResponse FromJson(string json) => JsonConvert.DeserializeObject<AgentResponse>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this AgentResponse self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}