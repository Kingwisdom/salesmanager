﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace SalesManagerBalanceResponseObject
{
    public partial class BalanceResponseObject
    {
        [JsonProperty("balanceId")]
        public Guid BalanceId { get; set; }

        [JsonProperty("amount")]
        public long Amount { get; set; }

        [JsonProperty("agentId")]
        public Guid AgentId { get; set; }
    }

    public partial class BalanceResponseObject
    {
        public static BalanceResponseObject FromJson(string json) => JsonConvert.DeserializeObject<BalanceResponseObject>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this BalanceResponseObject self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
