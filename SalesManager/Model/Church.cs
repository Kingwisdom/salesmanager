﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SQLite;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace SalesManagerModelChurch
{
    public class Church
    {
        [PrimaryKey,AutoIncrement]
        public int id { get; set; }
        
        public Guid LeadId { get; set; }

        public string ChurchName { get; set; }

        public string Address { get; set; }

        public string ChurchPhoneNumber { get; set; }

        public string Pastorsphonenumber { get; set; }

        public string Picture { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public double? Altitude { get; set; }

        public string ChurchContactName { get; set; }

        public string Website { get; set; }

        public string ChurchSize { get; set; }
        //public string SizeOfChurch { get; set; }

        public string Town { get; set; }

        public long Status { get; set; }

        public long Deal { get; set; }

        public Guid AgentId { get; set; }

        public DateTimeOffset ContactDate { get; set; }

        public DateTimeOffset DateVerified { get; set; }

        public string VerifiedBy { get; set; }

   
    }

    public partial class FollowUp
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public int Churchid { get; set; }
        public Guid FollowUpId { get; set; }

        public Guid LeadId { get; set; }

        public string Description { get; set; }

        public DateTimeOffset DateOfAction { get; set; }

        public string Remark { get; set; }

        public string FollowupBy { get; set; }
    }

  
}
