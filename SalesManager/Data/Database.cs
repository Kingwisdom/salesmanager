﻿using SalesManager.Model;
using SalesManagerModelChurch;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesManager.Data
{
    public static class DataBase
    {
        static readonly SQLiteConnection db;
        static DataBase()
        {
            db = new SQLiteConnection(App.DatabaseLocation);
            db.CreateTable<Church>();
        }

        public static void CloseDB()
        {
            db.Close();
        }
        public static int Addchurch(Church church)
        {
            return db.Insert(church);
        } 
        public static List<Church> Allchurches()
        {
            
            return db.Table<Church>().ToList();
        } 

        public static void clearall()
        {
            db.DeleteAll<Church>();
        }
    }
}
