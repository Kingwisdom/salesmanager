﻿using Newtonsoft.Json;
using SalesManager.Model.APIResponseObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace SalesManager.ViewModel
{
    public class LeadsDetailVM : BaseViewModel
    {
        public LeadsDetailVM(Guid Id)
        {
          var DashBordList =  JsonConvert.DeserializeObject<IEnumerable<AgentsLeadResObject>>(Application.Current.Properties["AgentsChurch"].ToString());
           ALRS = DashBordList.FirstOrDefault(e => e.LeadId == Id);
        }

        public AgentsLeadResObject ALRS
        {
            get;
            set;
        }
    }
}
