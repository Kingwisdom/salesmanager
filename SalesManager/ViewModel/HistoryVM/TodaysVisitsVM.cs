﻿using Newtonsoft.Json;
using SalesManager.Data;
using SalesManager.Model;
using SalesManager.Model.APIResponseObjects;
using SalesManagerAgentResponse;
using SalesManagerModelChurch;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SalesManager.ViewModel.HistoryVM
{
    class TodaysVisitsVM : BaseViewModel
    {
        public TodaysVisitsVM(int position)
        {
            //Allchurches = new ObservableCollection<Church>(DataBase.Allchurches());
            //VisitsToday = new ObservableCollection<Church>(DataBase.Allchurches().Where(a => a.ContactDate.Date == DateTime.Now.Date));
            //MonthsVisits = new ObservableCollection<Church>(DataBase.Allchurches().Where(a => a.ContactDate.Month == DateTime.Now.Month && a.ContactDate.Year == DateTime.Now.Year));

            switch(position)
            {
                case 1:
                    Case1TodaysVisits();
                    break;
                case 2:
                    Case2MonthsLeads();
                    break;
                case 3:
                    Case3AllLeads();
                    break;
                case 4:
                    Case4MonthSales();
                    break;
                case 5:
                    Case5AllSales();
                    break;
            }

     
        }

        private void Case1TodaysVisits()
        {
            Title = "Leads Acquired Today";
            var result = JsonConvert.DeserializeObject<IEnumerable<AgentsLeadResObject>>(Application.Current.Properties["AgentsChurch"].ToString());

            TotalLeads = new ObservableCollection<AgentsLeadResObject>(result.Where(c => c.DateVerified.Date == DateTime.Now.Date));
            Count = TotalLeads.Count();
        }
        private void Case2MonthsLeads()
        {
            Title = "Leads Acquired this Month";
             TotalLeads = new ObservableCollection<AgentsLeadResObject>(JsonConvert.DeserializeObject<IEnumerable<AgentsLeadResObject>>(Application.Current.Properties["AgentsSprint"].ToString()));
            Count = TotalLeads.Count();
        }  
        private void Case3AllLeads()
        {
            Title = "All Leads";
            TotalLeads = new ObservableCollection<AgentsLeadResObject>(JsonConvert.DeserializeObject<IEnumerable<AgentsLeadResObject>>(Application.Current.Properties["AgentsChurch"].ToString()));
            Count = TotalLeads.Count();
        } 
        private void Case4MonthSales()
        {
            Title = "Closed This Month";
            var result = JsonConvert.DeserializeObject<IEnumerable<AgentsLeadResObject>>(Application.Current.Properties["AgentsSprint"].ToString());
            AllSales = new ObservableCollection<AgentsLeadResObject>(result.Where(c => c.Deal == 1));
            Sales =  AllSales.Count;
        } 
        private void Case5AllSales()
        {
            Title = "All Closed";
            var result = JsonConvert.DeserializeObject<IEnumerable<AgentsLeadResObject>>(Application.Current.Properties["AgentsChurch"].ToString());
            AllSales = new ObservableCollection<AgentsLeadResObject>(result.Where(c => c.Deal == 1));
            Sales = AllSales.Count;
        }
                                                                                                                           
        public ObservableCollection<Church> Allchurches
        {
            get;
            set;
        } 
        
        public ObservableCollection<AgentsLeadResObject> AllSales
        {
            get;
            set;
        }

        public ObservableCollection<Church> VisitsToday
        {
            get;
            set;
        } 
        
        public ObservableCollection<AgentsLeadResObject> TotalLeads
        {
            get;
            set;
        } 
        
        public ObservableCollection<Church> MonthsVisits
        {
            get;
            set;
        }

        string title;
        public string Title
        {
            get => title;
            set
            {
                SetProperty(ref title, value);
            }

        }

        int count;
        public int Count
        {
            get => count;
            set
            {
                SetProperty(ref count, value);
            }
           
        } 
        
        long sales;
        public long Sales
        {
            get => sales;
            set
            {
                SetProperty(ref sales, value);
            }
           
        } 
        //public int MonthCount
        //{
        //    get => MonthsVisits.Count();
        //} 

        //public int TotalCount
        //{
        //    get => Allchurches.Count();
        //}
    }
}
