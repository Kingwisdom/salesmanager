﻿using System;
using SkiaSharp;
using System.Collections.Generic;
using System.Text;
using Microcharts;
using Entry = Microcharts.ChartEntry; 
using SalesManager.Data;
using System.Linq;
using Xamarin.Essentials;
using System.Windows.Input;
using Xamarin.Forms;
using SalesManager.View.ViewHistory;
using System.Threading.Tasks;
using SalesManagerModelChurch;
using SalesManager.Services.AgentServices;
using SalesManagerBalanceResponseObject;
using Newtonsoft.Json;
using SalesManager.View.Withdraw;
using System.Globalization;
using SalesManagerAgentResponse;
using SalesManager.Model.APIResponseObjects;
using SalesManager.View.Authentication;
using System.Collections.ObjectModel;
using FFImageLoading.Forms.Args;
using SalesManager.View;

namespace SalesManager.ViewModel
{
    class DashBoardVM : BaseViewModel
    {
        string myValue;

        IAgentService agentService;
        public DashBoardVM()
        {
            agentService = new AgentService();
            myValue = Preferences.Get("Username", null);
            _church = new List<Church>(DataBase.Allchurches());
            
            inti();
            MessagingCenter.Subscribe<ChurchVisitedVM>(this, "Done", (sender) => { });
            if (Application.Current.Properties.ContainsKey("AgentsChurch"))
            {
                getrecents();
            }
         
        }

        public void getrecents()
        {
            Allleads = new ObservableCollection<AgentsLeadResObject>(JsonConvert.DeserializeObject<IEnumerable<AgentsLeadResObject>>(Application.Current.Properties["AgentsChurch"].ToString()));
            recentleads = new ObservableCollection<AgentsLeadResObject>();
            for (int i = 0; i < Allleads.Count; i++)
            {
                if(i == 5)
                {
                    break;
                }
                recentleads.Add(Allleads[i]);
            }
        }

        // Make sure the values are not zero
        private void inti()
        {
            if (Preferences.ContainsKey("TotalVisits"))
            {
                TotalVisits = Preferences.Get("TotalVisits", 0);
            }
            if (Preferences.ContainsKey("strBalance"))
            {
                Balance = Preferences.Get("strBalance", null);
            }
            if (Preferences.ContainsKey("Sales"))
            {
                Sales = Preferences.Get("Sales", 0L);
            }
            if (Preferences.ContainsKey("Salessprint"))
            {
                Salessprint = Preferences.Get("Salessprint", 0L);
            }
            if (Preferences.ContainsKey("Leadssprint"))
            {
                Leadssprint = Preferences.Get("Leadssprint", 0L);
            }

        }
        List<Entry> entries => new List<Entry>
               {
                new Entry((float)Sales)
                {
                Label = "Sales",
                ValueLabel = Salessprint.ToString(),
                Color = SKColor.Parse("#F07221")
                },
                new Entry((float)Leadssprint)
                {
                Label = "Leads",
                ValueLabel = Leadssprint.ToString(),
                Color = SKColor.Parse("#0FAAB2")
                },
              
            };

        public DonutChart chart
        {
            get
            {
                var DC = new DonutChart()
                {
                    Entries = entries,
                };
                return DC;
            }
        }

        //  List<Church> _church;
        public List<Church> _church
        {
            get;
            set;
        }

        int _visitstoday;
        public int VisitsToday
        {
            get
            {

                var Todaysvisiits = _church.Where(a => a.ContactDate.Date == DateTime.Now.Date).ToList();
                return Todaysvisiits.Count();
            }
            set => SetProperty(ref _visitstoday, value);
        }

        private long sales;

        public long Sales
        {
            get => sales;
            set => SetProperty(ref sales, value);
        }
        
        private long salessprint;

        public long Salessprint
        {
            get => salessprint;
            set => SetProperty(ref salessprint, value);
        }   
        
        private long leadssprint;

        public long Leadssprint
        {
            get => leadssprint;
            set => SetProperty(ref leadssprint, value);
        } 
        
        private bool refresh;

        public bool Refresh
        {
            get => refresh;
            set => SetProperty(ref refresh, value);
        }

        public bool MonthSales
        {
            get
            {
                if (Sales >= 5)
                {
                    return true;
                }
                return false;
            }
        }
        public int MonthVisit
        {
            get
            {
                var Visitsthismonth = _church.Where(a => a.ContactDate.Month == DateTime.Now.Month && a.ContactDate.Year == DateTime.Now.Year);
                return Visitsthismonth.Count();
            }
        }
        public bool MonthComplete
        {
            get
            {
                if (VisitsToday >= 30)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool TodayComplete
        {
            get
            {
                if (VisitsToday >= 4)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        ObservableCollection<AgentsLeadResObject> recentleads;
        public ObservableCollection<AgentsLeadResObject> Recentleads
        {
            get => recentleads;
            set => SetProperty(ref recentleads,value);
        }
        
        public ObservableCollection<AgentsLeadResObject> Allleads
        {
            get;
            set;
        }

        int totalVisits;
        public int TotalVisits
        {
            get => totalVisits;

            set => SetProperty(ref totalVisits, value);
        }

        public string Username
        {
            get
            {
                return myValue;
            }
        }

        string _balance;
        public string Balance
        {
            get => _balance;
            set => SetProperty(ref _balance, value);
        }

        public ICommand SprintSales => new Command(() => Application.Current.MainPage.Navigation.PushAsync(new OverallAllSales(4))); 
        public ICommand Summary => new Command(() => Application.Current.MainPage.Navigation.PushAsync(new Summary()));
        public ICommand CheckToday => new Command(() => Application.Current.MainPage.Navigation.PushAsync(new TodaysVisits(3)));
        public ICommand CheckMonth => new Command(async() => await Application.Current.MainPage.Navigation.PushAsync(new MonthsVisits(2)));
        public ICommand NavToBalance => new Command(() => Application.Current.MainPage.Navigation.PushAsync(new WithdrawalPage()));
        public ICommand Logout => new Command(() =>
        {
            Preferences.Set("Username", null);
            Application.Current.Properties.Remove("AgentsChurch");
            Application.Current.MainPage = new NavigationPage(new LoginviewMain());
        });

        public ICommand IsRefreshing => new Command(async() =>
        {
            if(Connectivity.NetworkAccess == NetworkAccess.Internet)
            {
                try
                {
                    Refresh = true;
                    await GetBalance();
                    await GetAgentbysprint();
                    await GetAgentChurch();
                    if (Application.Current.Properties.ContainsKey("AgentsChurch"))
                    {
                        getrecents();
                    }
                    Refresh = false;
                }
                catch
                {

                }
            }
            else
            {
               await Application.Current.MainPage.DisplayAlert("", "No internet connection", "Ok");
            }
        });
        //public async Task GetAgent()
        //{
        //        var success = await agentService.Getagent<AgentResponse>();
        //        if (success)
        //        {
        //            var result = JsonConvert.DeserializeObject<AgentResponse>(Application.Current.Properties["Agent"].ToString());
        //            Sales = result.NumberOfLeads;
        //            Preferences.Set("Sales", Sales);
        //        }
        //}
        
        public async Task GetAgentbysprint()
        {
                var success = await agentService.Getagentsprint<AgentsLeadResObject>();
                if (success)
                {
                    var result = JsonConvert.DeserializeObject<IEnumerable<AgentsLeadResObject>>(Application.Current.Properties["AgentsSprint"].ToString());
                    Leadssprint = result.Count();
                   var allSales = result.Where(c => c.Deal == 1);
                    Salessprint = allSales.Count();
                    Preferences.Set("Salessprint", Salessprint);
                    Preferences.Set("Leadssprint", Leadssprint);
                }
        } 
        
        public async Task GetAgentChurch()
        {
                var success = await agentService.Getagentschurch<AgentsLeadResObject>();
                if (success)
                {
                     var result = JsonConvert.DeserializeObject<IEnumerable<AgentsLeadResObject>>(Application.Current.Properties["AgentsChurch"].ToString());
                     var list = result.Where(c => c.Status == 1);
                     var allSales = result.Where(c => c.Deal == 1);
                     TotalVisits = list.Count();
                     Sales = allSales.Count();
                     Preferences.Set("Sales", Sales);
                     Preferences.Set("TotalVisits", TotalVisits);
                }
        } 
        
        public async Task GetBalance()
        {
            RegionInfo myRi1 = new RegionInfo("NG");
               var success = await agentService.GetBalance<BalanceResponseObject>();
                if (success)
                {
                    var result = JsonConvert.DeserializeObject<BalanceResponseObject>(Application.Current.Properties["CurrentBalance"].ToString());
                    long longbalance = result.Amount;
                    Balance = myRi1.CurrencySymbol + string.Format("{0:N}", result.Amount);
                    Preferences.Set("strBalance", Balance);
                    Preferences.Set("Balance", longbalance);
                }
       
        }
    }
}
