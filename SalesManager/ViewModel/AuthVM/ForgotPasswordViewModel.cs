﻿using SalesManager.Services;
using SalesManager.Services.AuthServices;
using SalesManagerForgotPasswordResponse;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SalesManager.ViewModel.AuthVM
{
    class ForgotPasswordViewModel : BaseViewModel
    {
        INavigation navigation;
        ILoginService loginService;
        public ForgotPasswordViewModel(INavigation navigation)
        {
            this.navigation = navigation;
           loginService = new loginServices(); ;
            IsNotBusy = true;
        }

        public string email;
        public string Email
        {
            get => email;

            set
            {
                SetProperty(ref email, value);
            }
        }

        public ICommand SubmitCommand => new Command(() =>
        {
            IsNotBusy = false;
            loginService.forgotPassword<ForgotPasswordResponse>(email);
            IsBusy = true;
        });
    }
}
