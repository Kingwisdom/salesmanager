﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesManager.Constants
{
    public static class ApiConstants
    {
      public const string BaseURL = "http://agentnetwork.azurewebsites.net/api/";

      public const string login = BaseURL + "Account/login";

        public const string forgotpassword = BaseURL + "Account/forgotpassword";

        public const string Agent = BaseURL + "Agent/";

        public const string CreateLead = BaseURL + "Lead";

        public const string Balance = BaseURL + "Balance/agentbalance?AgentId=";

        public const string GetagentLead = BaseURL + "/Lead/leadsbyagent?AgentId=";  
        
        public const string Getagentssprint = BaseURL + "/Lead/leadsinsprint?AgentId=";

        public const string withdraw = BaseURL + "Withdrawer";

    }
}
